# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import settings
from .models import Event, EventCategory
from django.contrib.sitemaps import Sitemap
from django.core.paginator import Paginator

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class EventSitemap(Sitemap):

    model = Event

    def items(self):
        return self.model.objects.current_site().indexable()

    def lastmod(self, obj):
        return obj.updated_at


class EventListSitemap(Sitemap):

    model = Event

    def items(self):
        objects = self.model.objects.current_site().active()
        page_size = settings.EVENT_PAGINATE_BY
        if objects:
            return Paginator(objects, page_size).page_range if \
                page_size else [1]
        else:
            return [1] if settings.ALLOW_EMPTY_EVENT_LIST else []

    def location(self, page):
        if page == 1:
            return reverse('events:event_list')
        else:
            return reverse('events:event_list', kwargs={'page': page})


class CategoryEventListSitemap(Sitemap):

    model = EventCategory

    def items(self):
        categories = self.model.objects.indexable()
        items = []
        for category in categories:
            objects = category.events.current_site().active()
            page_size = settings.EVENT_PAGINATE_BY
            if objects:
                page_range = Paginator(objects, page_size).page_range if \
                    page_size else [1]
            else:
                page_range = [1] if settings.ALLOW_EMPTY_EVENT_LIST else []
            items += [(category.slug, page) for page in page_range]
        return items

    def location(self, opts):
        slug, page = opts
        if page == 1:
            return reverse('events:event_list', kwargs={'slug': slug})
        else:
            return reverse('events:event_list', kwargs={'slug': slug,
                                                        'page': page})


sitemaps = {'event_event': EventSitemap,
            'event_eventlist': EventListSitemap,
            'event_categoryeventlist': CategoryEventListSitemap}
