# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Event, EventCategory
from django.contrib import admin
from thecut.authorship.admin import AuthorshipMixin


@admin.register(Event)
class EventAdmin(AuthorshipMixin, admin.ModelAdmin):

    date_hierarchy = 'start_at'

    fieldsets = [
        (None, {'fields': ['title', 'headline', 'start_at', 'end_at',
                           'featured_content', 'content', 'meta_description',
                           'tags']}),
        ('Publishing', {'fields': ['categories', 'site', 'slug',
                                   ('publish_at', 'is_enabled'), 'expire_at',
                                   'publish_by', 'template', 'is_featured',
                                   'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['title', 'start_at', 'end_at', 'publish_at', 'is_enabled',
                    'is_featured', 'is_indexable']

    list_filter = ['categories', 'start_at', 'end_at', 'publish_at',
                   'is_enabled', 'is_featured', 'is_indexable', 'site']

    prepopulated_fields = {'slug': ['title']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']


@admin.register(EventCategory)
class EventCategoryAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', 'headline', 'featured_content', 'content',
                           'meta_description', 'tags']}),
        ('Publishing', {'fields': ['slug', ('publish_at', 'is_enabled'),
                                   'expire_at', 'publish_by', 'template',
                                   'is_featured', 'is_indexable',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['title', 'publish_at', 'is_enabled', 'is_featured',
                    'is_indexable']

    list_filter = ['publish_at', 'is_enabled', 'is_featured', 'is_indexable']

    prepopulated_fields = {'slug': ['title']}

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']

    search_fields = ['title']
