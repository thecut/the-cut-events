# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings


ALLOW_EMPTY_EVENT_LIST = getattr(settings, 'EVENTS_ALLOW_EMPTY_EVENT_LIST',
                                 True)

EVENT_PAGINATE_BY = getattr(settings, 'EVENTS_EVENT_PAGINATE_BY', 10)
