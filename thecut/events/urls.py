# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import feeds, views
from django.conf.urls import include, url


urls = [

    url(r'^$',
        views.ListView.as_view(), name='event_list'),
    url(r'^(?P<page>\d+)$',
        views.ListView.as_view(), name='event_list'),
    url(r'^(?P<slug>[\w-]+)/$',
        views.ListView.as_view(), name='event_list'),
    url(r'^(?P<slug>[\w-]+)/(?P<page>\d+)$',
        views.ListView.as_view(), name='event_list'),

    url(r'^latest\.xml$',
        feeds.LatestEventFeed(), name='event_feed'),
    url(r'^(?P<slug>[\w-]+)/latest\.xml$',
        feeds.LatestCategoryEventFeed(), name='event_feed'),

    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[\w-]+)$',
        views.DetailView.as_view(), name='event_detail'),

    # These old url names are kept for backwards-compatibility
    url(r'^(?P<page>\d+)$',
        views.ListView.as_view(), name='paginated_event_list'),
    url(r'^(?P<slug>[\w-]+)/$',
        views.ListView.as_view(), name='category_event_list'),
    url(r'^(?P<slug>[\w-]+)/(?P<page>\d+)$',
        views.ListView.as_view(), name='paginated_category_event_list'),
    url(r'^(?P<slug>[\w-]+)/latest\.xml$',
        feeds.LatestCategoryEventFeed(), name='category_event_feed'),

]

urlpatterns = [
    url(r'^', include(urls, namespace='events')),
]
