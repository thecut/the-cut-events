# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import querysets
from django.conf import settings
from django.db import models
from django.utils import timezone
from thecut.publishing.models import Content, SiteContent
from thecut.publishing.utils import generate_unique_slug

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class AbstractEvent(SiteContent):
    """Generic event."""

    slug = models.SlugField(unique_for_date='start_at')

    start_at = models.DateTimeField(
        'start date & time', db_index=True,
        help_text='The start date & time of this event.')

    end_at = models.DateTimeField(
        'end date & time', db_index=True, blank=True, null=True,
        help_text='Optional end date & time of the event.')

    objects = querysets.AbstractEventQuerySet.as_manager()

    class Meta(SiteContent.Meta):
        abstract = True
        ordering = ['start_at']
        unique_together = ['site', 'start_at', 'slug']

    def get_next(self):
        return self.__class__.objects.exclude(pk=self.pk).filter(
            site=self.site, start_at__gte=self.start_at).active().order_by(
            'start_at').first()

    def get_previous(self):
        return self.__class__.objects.exclude(pk=self.pk).filter(
            site=self.site, start_at__lte=self.start_at).active().order_by(
            '-start_at').first()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_unique_slug(
                self.title,
                self.__class__.objects.filter(
                    site=self.site,
                    start_at__year=self.start_at.year,
                    start_at__month=self.start_at.month,
                    start_at__day=self.start_at.day))
        return super(AbstractEvent, self).save(*args, **kwargs)


class AbstractEventCategory(Content):

    slug = models.SlugField(unique=True)

    class Meta(Content.Meta):
        abstract = True
        verbose_name_plural = 'event categories'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_unique_slug(self.title,
                                             self.__class__.objects.all())
        return super(AbstractEventCategory, self).save(*args, **kwargs)


class Event(AbstractEvent):

    categories = models.ManyToManyField('events.EventCategory',
                                        related_name='events', blank=True)

    def get_absolute_url(self):

        # Ensure consistent urls are determined from the server's timezone.

        server_timezone = timezone.pytz.timezone(settings.TIME_ZONE)

        if timezone.is_naive(self.publish_at):
            start_at = server_timezone.localize(self.start_at)
        else:
            start_at = server_timezone.normalize(self.start_at)

        return reverse('events:event_detail',
                       kwargs={'year': '%04d' % (start_at.year),
                               'month': '%02d' % (start_at.month),
                               'day': '%02d' % (start_at.day),
                               'slug': self.slug})


class EventCategory(AbstractEventCategory):

    def get_absolute_url(self):
        return reverse('events:event_list', kwargs={'slug': self.slug})
