# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import settings
from .models import Event, EventCategory
from django.shortcuts import get_object_or_404, redirect
from django.views import generic


class CurrentEventsMixin(object):

    _event_model = Event

    current_events_limit = None

    def _get_current_events_queryset(self):
        queryset = self._event_model.objects.current_site().active().current()
        if self.current_events_limit is not None:
            queryset = queryset[:self.current_events_limit]
        return queryset

    def get_context_data(self, *args, **kwargs):
        context_data = super(CurrentEventsMixin, self).get_context_data(
            *args, **kwargs)
        context_data.update({
            'current_events_list': self._get_current_events_queryset()})
        return context_data


class FutureEventsMixin(object):

    _event_model = Event

    future_events_limit = None

    def _get_future_events_queryset(self):
        queryset = self._event_model.objects.current_site().active().future()
        if self.future_events_limit is not None:
            queryset = queryset[:self.future_events_limit]
        return queryset

    def get_context_data(self, *args, **kwargs):
        context_data = super(FutureEventsMixin, self).get_context_data(
            *args, **kwargs)
        context_data.update({
            'future_events_list': self._get_future_events_queryset()})
        return context_data


class DetailView(generic.DateDetailView):

    allow_future = True

    context_object_name = 'event'

    date_field = 'start_at'

    model = Event

    month_format = '%m'

    template_name_field = 'template'

    def get_queryset(self, *args, **kwargs):
        queryset = super(DetailView, self).get_queryset(*args, **kwargs)
        return queryset.current_site().active()


class ListView(generic.ListView):

    allow_empty = settings.ALLOW_EMPTY_EVENT_LIST

    category_model = EventCategory

    context_object_name = 'event_list'

    model = Event

    paginate_by = settings.EVENT_PAGINATE_BY

    queryset_method = None  # 'past', 'current', 'future'

    def __init__(self, *args, **kwargs):
        # Backwards compatibility for ``upcoming`` property
        if self.queryset_method is None and getattr(self, 'upcoming', False):
            self.queryset_method = 'future'
        return super(ListView, self).__init__(*args, **kwargs)

    def get(self, *args, **kwargs):
        page = self.kwargs.get('page', None)
        if page is not None and int(page) < 2:
            category = self.get_category()
            if category:
                return redirect('events:event_list', slug=category.slug,
                                permanent=True)
            else:
                return redirect('events:event_list', permanent=True)
        return super(ListView, self).get(*args, **kwargs)

    def get_category(self):
        if not hasattr(self, '_category'):
            slug = self.kwargs.get('slug', None)
            if slug is not None:
                category = get_object_or_404(
                    self.category_model.objects.active(), slug=slug)
            else:
                category = None
            self._category = category
        return self._category

    def get_context_data(self, *args, **kwargs):
        context_data = super(ListView, self).get_context_data(*args, **kwargs)
        category = self.get_category()
        context_data.setdefault('category', category)
        return context_data

    def get_queryset(self, *args, **kwargs):
        queryset = super(ListView, self).get_queryset(*args, **kwargs)
        category = self.get_category()
        if category:
            queryset = queryset.filter(categories=category)
        if self.queryset_method is not None:
            queryset = getattr(queryset, self.queryset_method)()
        return queryset.current_site().active()

    def get_template_names(self, *args, **kwargs):
        template_names = super(ListView, self).get_template_names(*args,
                                                                  **kwargs)
        category = self.get_category()
        if category and category.template:
            template_names = [category.template] + template_names
        return template_names
