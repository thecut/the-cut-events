# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import models
from django.utils import timezone
from thecut.publishing.querysets import SiteContentQuerySet


class AbstractEventQuerySet(SiteContentQuerySet):

    """Customised :py:class:`~django.db.models.db.query.QuerySet` for
    :py:class:`~thecut.events.models.AbstractEvent` model."""

    def current(self):
        """Filter for events which have started but not yet ended.

        :returns: Filtered queryset.
        :rtype: :py:class:`.AbstractEventQuerySet`

        """

        now = timezone.now()
        return self.filter(start_at__lte=now, end_at__gte=now)

    def future(self):
        """Filter for events which have not yet started.

        :returns: Filtered queryset.
        :rtype: :py:class:`.AbstractEventQuerySet`

        """

        now = timezone.now()
        return self.filter(start_at__gt=now)

    def past(self):
        """Filter for events which have ended.

        :returns: Filtered queryset.
        :rtype: :py:class:`.AbstractEventQuerySet`

        """

        now = timezone.now()
        return self.filter(
            models.Q(end_at__lte=now) |
            models.Q(end_at__isnull=True, start_at__lte=now))

    def upcoming(self):
        # Backwards compatibility for ``upcoming`` method
        return self.future()
