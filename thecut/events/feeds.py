# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .models import Event, EventCategory
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from django.utils.feedgenerator import Atom1Feed

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class LatestEventFeed(Feed):

    feed_type = Atom1Feed

    model = Event

    subtitle = 'Latest events.'

    def link(self):
        return reverse('events:event_list')

    def title(self):
        site = Site.objects.get_current()
        return '{} - Events'.format(site.name)

    def items(self):
        return self.model.objects.current_site().active().order_by(
            '-publish_at')[:10]

    def item_title(self, item):
        return '{}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at


class LatestCategoryEventFeed(Feed):

    feed_type = Atom1Feed

    model = EventCategory

    def title(self, obj):
        return 'Latest {}'.format(obj)

    def link(self, obj):
        return reverse('events:event_list', kwargs={'slug': obj.slug})

    def get_object(self, request, slug):
        return get_object_or_404(self.model, slug=slug)

    def items(self, obj):
        return obj.events.current_site().active().order_by(
            '-publish_at')[:10]

    def item_title(self, item):
        return '{}'.format(item)

    def item_description(self, item):
        return item.content

    def item_pubdate(self, item):
        return item.publish_at
